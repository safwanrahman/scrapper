from django.shortcuts import render
from rest_framework import generics

# Create your views here.
from django_filters import rest_framework as filters
from .models import Url, Image
from .serializers import UrlSerializer, ImageSerializer, ImageMetadataSerializer


class UrlCreateView(generics.CreateAPIView):
    serializer_class = UrlSerializer


class ImageListView(generics.ListAPIView):
    serializer_class = ImageSerializer
    queryset = Image.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ("id", "url__url")


class ImageMetadataView(generics.ListAPIView):
    serializer_class = ImageMetadataSerializer
    queryset = Image.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ("id", "url__url")
