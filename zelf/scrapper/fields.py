from rest_framework.fields import ImageField
from sorl.thumbnail import get_thumbnail

from .config import IMAGE_SIZE_CONFIG


class MultiSizeImageField(ImageField):
    def to_representation(self, value):
        request = self.context.get("request")

        if value and request and request.query_params.get("size"):
            size = request.query_params.get("size")
            actual_size = IMAGE_SIZE_CONFIG.get(size)
            if actual_size and value.width > actual_size:
                value = get_thumbnail(value, str(actual_size))

        return super().to_representation(value)
