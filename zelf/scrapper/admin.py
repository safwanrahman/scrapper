from django.contrib import admin
from .models import Url, Image

# Register your models here.

admin.site.register(Url)
admin.site.register(Image)
