from django.urls import path

from zelf.scrapper.views import UrlCreateView, ImageListView, ImageMetadataView

urlpatterns = [
    path("urls/", UrlCreateView.as_view(), name="url-create"),
    path("images/", ImageListView.as_view(), name="image-list"),
    path("images/metadata", ImageMetadataView.as_view(), name="image-metadata"),
]
