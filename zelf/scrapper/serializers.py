from rest_framework.serializers import ModelSerializer, ImageField

from .fields import MultiSizeImageField
from .models import Url, Image


class ImageSerializer(ModelSerializer):
    image = MultiSizeImageField(read_only=True)

    class Meta:
        model = Image
        fields = ["image_url", "image"]


class UrlSerializer(ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Url
        fields = ["url", "images"]

    def create(self, validated_data):
        images = validated_data.pop("images")
        obj = super().create(validated_data)
        for image_data in images:
            Image.objects.create(**image_data, url=obj)

        return obj


class ImageMetadataSerializer(ModelSerializer):
    class Meta:
        model = Image
        fields = ["scrapping_date", "image_size", "image_dimension"]
