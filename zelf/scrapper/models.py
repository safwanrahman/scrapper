import tempfile

import requests
from django.core import files
from django.core.files.temp import NamedTemporaryFile
from django.db import models, transaction
from PIL import Image as PilImage
from django.utils import timezone

from zelf.scrapper.tasks import fetch_images_task


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Url(BaseModel):
    url = models.URLField()


class Image(BaseModel):
    url = models.ForeignKey(Url, related_name="images", on_delete=models.CASCADE)
    image_url = models.URLField()
    image = models.ImageField(blank=True, null=True)
    scrapping_date = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        created = not self.pk
        super().save(*args, **kwargs)
        if created:
            transaction.on_commit(self.issue_task_to_fetch_image)

    @property
    def image_size(self):
        return self.image.size

    @property
    def image_dimension(self):
        return f"{self.image.width}x{self.image.height}"

    def issue_task_to_fetch_image(self):
        fetch_images_task.delay(self.id)

    def fetch_image_object(self):
        resp = requests.get(self.image_url)
        if resp.status_code != requests.codes.ok:
            return None

        file = NamedTemporaryFile()
        file.write(resp.content)
        pil_image = PilImage.open(file)
        image_name = self.image_url.split("/")[-1]
        file_name = f"{image_name}.{pil_image.format}"
        self.image.save(file_name, files.File(file))
        self.scrapping_date = timezone.now()
        self.save()
