from ..celery import app
from django.apps import apps


@app.task
def fetch_images_task(image_id):
    Image = apps.get_model("scrapper", "Image")
    image = Image.objects.get(id=image_id)
    image.fetch_image_object()
